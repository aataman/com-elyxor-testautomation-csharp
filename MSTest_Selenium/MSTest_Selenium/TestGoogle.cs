﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace MSTest_Selenium
{
    [TestClass]
    public class TestGoogle
    {

        [TestMethod]
        public void TestGoogleTitle()
        {
            var driver = new ChromeDriver
            {
                Url = "http://www.google.com"
            };

            Assert.IsTrue(driver.Title == "Google");
            driver.Dispose();

        }

        [TestMethod]
        public void TestGoogleSearch()
        {
            var driver = new ChromeDriver
            {
                Url = "http://www.google.com"
            };

            IWebElement element = driver.FindElement(By.Name("q"));

            element.SendKeys("Fenerbahce");
            driver.Dispose();

        }
    }
}
