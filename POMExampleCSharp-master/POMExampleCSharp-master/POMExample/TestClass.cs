﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using POMExample.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample
{
    public class TestClass
    {

        private IWebDriver driver;

        [TestCase]
        public void CheckHomePageTitle()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();


            HomePage home = new HomePage(driver);
            home.goToHomePage();
            StringAssert.Contains("Tennis Express", driver.Title);

            driver.Dispose();
        }

        [TestCase]
        public void CheckOtherPage()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();


            HomePage home = new HomePage(driver);
            home.goToHomePage();
            home.goToOtherPage();
            OtherPage other = new OtherPage(driver);

            Assert.True(other.isPageOpened());
            driver.Dispose();
        }

        [TestCase]
        public void CreateAccount()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();


            HomePage home = new HomePage(driver);
            home.goToHomePage();
            home.goToSignInPage();
            SignInPage signIn = new SignInPage(driver);

            signIn.setEmail("mytest1@adsf.com");
            signIn.setPassword("12342afsda");
            signIn.clickRegister();
            home.goToHomePage();

            driver.Dispose();
        }
    }
}
