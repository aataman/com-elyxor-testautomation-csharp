﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample.PageObjects
{
    class OtherPage
    {
        private IWebDriver driver;
        private WebDriverWait wait;

        [FindsBy(How = How.TagName, Using = "h1")]
        private IWebElement header;

        public OtherPage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            PageFactory.InitElements(driver, this);
        }


        public bool isPageOpened()
        {
            return header.Text.Contains("Other");
        }

    }
}