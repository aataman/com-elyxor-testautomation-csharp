﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample.PageObjects
{
    class HomePage
    {

        private IWebDriver driver;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = ".web-nav a[href*='/other-tennis-accessories']")]
        private IWebElement other;

        [FindsBy(How = How.CssSelector, Using = ".header-signin")]
        private IWebElement signIn;


        public void goToHomePage()
        {
            driver.Navigate().GoToUrl("https://www.tennisexpress.com/");
        }

        public OtherPage goToOtherPage()
        {
            other.Click();
            return new OtherPage(driver);
        }

        public SignInPage goToSignInPage()
        {
            signIn.Click();
            return new SignInPage(driver);
        }


    }
}
