﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample.PageObjects
{
    class SignInPage
    {

        private IWebDriver driver;

        public SignInPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);            
        }

        [FindsBy(How = How.Name, Using = "emailAddressReg")]
        private IWebElement emailInput;

        [FindsBy(How = How.Name, Using = "passwrdReg")]
        private IWebElement passInput;

        [FindsBy(How = How.Name, Using = "register")]
        private IWebElement registerButton;


        public void setEmail(string email)
        {
            emailInput.SendKeys(email);
        }

        public void setPassword(string password)
        {
            passInput.SendKeys(password);
        }

        public void clickRegister()
        {
            registerButton.Click();
        }

    }
}
