﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit_Bank;
using NUnit.Framework;

namespace TestBank
{
    [TestFixture]
    public class TestBank
    {

        [TestCase]
        public void TestSpending()
        {
            Bank banker = new Bank(10);
            banker.ReceiveSpending(3);

            Assert.AreEqual(7, banker.CurrentAmount);
        }

        [TestCase]
        public void TestNegativeSpendingDoesNotChangeCurrentAmount()
        {
            Bank banker = new Bank(10);
            banker.ReceiveSpending(-3);

            Assert.AreEqual(10, banker.CurrentAmount);
        }
    }
}