﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnit_Bank
{
    public class Bank
    {
        public int CurrentAmount { get; private set; }

        public Bank(int currentAmount)
        {
            CurrentAmount = currentAmount;
        }

        public void ReceiveSpending(int amountSpent)
        {
            if (amountSpent < 0)
                CurrentAmount = CurrentAmount;
            else
                CurrentAmount = (CurrentAmount - amountSpent);

        }
    }
}
