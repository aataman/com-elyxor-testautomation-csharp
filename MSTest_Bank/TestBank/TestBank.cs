﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTest_Bank;

namespace TestBank
{
    [TestClass]
    public class TestBank
    {
        [TestMethod]
        public void TestSpending()
        {
            Bank banker = new Bank(10);
            banker.ReceiveSpending(3);

            Assert.AreEqual(7, banker.CurrentAmount);
        }

        [TestMethod]
        public void TestNegativeSpendingDoesNotChangeCurrentAmount()
        {
            Bank banker = new Bank(10);
            banker.ReceiveSpending(-3);

            Assert.AreEqual(10, banker.CurrentAmount);
        }
    }
}
