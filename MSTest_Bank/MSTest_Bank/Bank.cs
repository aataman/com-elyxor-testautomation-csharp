﻿using System;

namespace MSTest_Bank
{
    public class Bank
    {
        public int CurrentAmount { get; private set; }

        public Bank(int currentAmount)
        {
            CurrentAmount = currentAmount;
        }

        public void ReceiveSpending(int amountSpent)
        {
            if (amountSpent < 0)
                CurrentAmount = CurrentAmount;
            else
                CurrentAmount = (CurrentAmount - amountSpent);

        }
    }
}
