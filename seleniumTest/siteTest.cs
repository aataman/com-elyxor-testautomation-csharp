﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace seleniumTest
{
    class siteTest
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            // Version 2.28 seems to be the one that works with no security pop ups. 
            driver = new ChromeDriver("c:\\googleDriver");
        }

        [Test]
        public void test()
        {
            // Actual Test
            driver.Navigate().GoToUrl("http://www.google.com");
            StringAssert.Contains("Google", driver.Title);

            // Not necessary, but proves that we are able to find elements and sendKeys to create a search
            IWebElement query = driver.FindElement(By.Name("q"));
            query.SendKeys("Fenerbahce");
            query.Submit();
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Close();
        }
    }
}
